
import Data.List

{- Problem 1

(*) Find the last element of a list.

(Note that the Lisp transcription of this problem is incorrect.)

Example in Haskell:

Prelude> myLast [1,2,3,4]
4
Prelude> myLast ['x','y','z']
'z'
-}
myLast :: [a] -> a
myLast [] = error "Empy list has no last item"
myLast [x] = x
myLast (_:xs) = myLast xs


{- Problem 2

(*) Find the last but one element of a list.

(Note that the Lisp transcription of this problem is incorrect.)

Example in Haskell:

Prelude> myButLast [1,2,3,4]
3
Prelude> myButLast ['a'..'z']
'y'
-}
myButLast :: [a] -> a
myButLast [] = error "Empy list has no second last item"
myButLast [x] = error "Single element list has no second last item"
myButLast [x,y] = x
myButLast (x:xs) = myButLast xs


{- Problem 3 

Find the K'th element of a list. The first element in the list is number 1.

Example:

* (element-at '(a b c d e) 3)
c

Example in Haskell:

Prelude> elementAt [1,2,3] 2
2
Prelude> elementAt "haskell" 5
'e'

-}
elementAt :: [a] -> Int -> a
elementAt [] _ = error "Empty list"
elementAt lst n = elementAt' lst n 1

elementAt' :: [a] -> Int -> Int -> a
elementAt' (x:xs) n i =
	if n == i then
		x
	else
		elementAt' xs n (i + 1)


main =
	putStrLn "Start..."
	-- Problem 1
	>> putStrLn (show $ myLast [1,2,3,4])
	>> putStrLn (show $ myLast ['a'..'z'])

	-- Problem 2
	>> putStrLn (show $ myButLast [1,2,3,4])
	>> putStrLn (show $ myButLast ['a'..'z'])

	-- Problem 3
	>> putStrLn (show $ elementAt [1,2,3] 2)
	>> putStrLn (show $ elementAt "haskell" 5)

	>> putStrLn "End."
